# Available images

## Sign&go server images

<table cellpadding="10" border="1" >
    <thead>
        <tr>
            <th align="left">Image name<br/>[IMAGE_NAME]</th>
            <th align="left">Description</th>
            <th align="left">Note</th>
            <th align="left">Available tags<br/>[TAG]</th>
            <th align="left">Supported architectures</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>signandgo-webserver-full</td>
            <td>Sign&go WebServer image</td>
            <td>
                Contains Sign&go Web server ADMIN and Sign&go Web server USER images, i.e., all administration and user functionalities. See below for more details.
            </td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
        <tr>
            <td>signandgo-webserver-admin</td>
            <td>Sign&go WebServer ADMIN image</td>
            <td>Contains all administration functionalities such as Configuration, Audit, Partnership, etc.</td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
        <tr>
            <td>signandgo-webserver-user</td>
            <td>Sign&go WebServer USER image</td>
            <td>Contains all user functionalities such as authentication, Sponsorship, My account, etc.</td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
        <tr>
            <td>signandgo-secserver</td>
            <td>Sign&go SecurityServer image</td>
            <td>Contains all security server functionalities.</td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
    </tbody>
</table>

## Sign&go database images

<table cellpadding="10" border="1" >
    <thead>
        <tr>
            <th align="left">Image name<br/>[IMAGE_NAME]</th>
            <th align="left">Description</th>
            <th align="left">Note</th>
            <th align="left">Available tags<br/>[TAG]</th>
            <th align="left">Supported architectures</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>signandgo-db-logs-postgres</td>
            <td>Audit database for Sign&go [postgresql] image</td>
            <td>
                Contains database with initialization schemas.<br/>
                For further information, please refer to Documentation portal > Integration.
            </td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
        <tr>
            <td>signandgo-db-technical-postgres</td>
            <td>Technical directory database for Sign&go [postgresql] image</td>
            <td>
                Contains database with initialization schemas.<br/>
                For further information, please refer to Documentation portal > Integration.
            </td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
    </tbody>
</table>

# Sign&go components and images

<table cellpadding="10" border="1" >
    <thead>
        <tr>
            <th align="left">Name</th>
            <th align="left">Referenced image(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Security server</td>
            <td>Sign&go SecurityServer image</td>
        </tr>
        <tr>
            <td>Authentication and authorisation agents</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer ADMIN image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
        <tr>
            <td>Administration server</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer ADMIN image
            </td>
        </tr>
        <tr>
            <td>User directories</td>
            <td>
                Not supported by Ilex images<br/>
                Can be OpenLDAP, Active Directory (AD), etc.<br/>
                For further information, please refer to Documentation portal > Integration.
            </td>
        </tr>
        <tr>
            <td>Audit</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer ADMIN image
            </td>
        </tr>
        <tr>
            <td>Administration application</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer ADMIN image
            </td>
        </tr>
        <tr>
            <td>User portal</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
        <tr>
            <td>Mobility Center</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
        <tr>
            <td>Sponsorship</td>    
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
        <tr>
            <td>My account</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
    </tbody>
</table>
<br/>

# Customization by variables

## Sign&go SecurityServer

<table cellpadding="10" border="1"><thead><tr><th align="left">Name </th><th align="left">Description</th><th align="left">Default value</th></tr></thead><tbody><tr><td>CIPHER_CONFIG_KEYS_DIR</td><td>Storage directory for cipher keys.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(3) Used for password encryption.</td></tr><tr><td>DATASOURCE_CONFIG_DIALECT</td><td>JDBC dialect class.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(5) Value computed according to JDBC protocol.<br/>(6) See the supported dialects for JDBC protocol.</td></tr><tr><td>DATASOURCE_CONFIG_DRIVER</td><td>JDBC driver class. For further information, please refer to Documentation portal > Integration.</td><td>(5) Value computed according to JDBC protocol.</td></tr><tr><td>DATASOURCE_CONFIG_LOGIN</td><td>User login.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(5) Value computed according to JDBC protocol.</td></tr><tr><td>DATASOURCE_CONFIG_PWD</td><td>User password.<br/>For further information, please refer to Documentation portal > Integration.</td><td>Required parameter.</td></tr><tr><td>DATASOURCE_CONFIG_URL</td><td>JDBC URL.<br/>For further information, please refer to Documentation portal > Integration.</td><td>Required parameter.</td></tr><tr><td>JAVA_OPTS</td><td>JVM options.</td><td>UNDEFINED</td></tr><tr><td>JAVA_OPTS_XMS_SIZE</td><td>JVM XMS size (in bytes).</td><td>128M</td></tr><tr><td>JAVA_OPTS_XMX_SIZE</td><td>JVM XMX size (in bytes).</td><td>600M</td></tr><tr><td>LOGS_APPLICATION_ID</td><td>Application ID (logs persistent files).</td><td>The FQDN of current container.<br/>Shall be unique when audit persistent folder is shared between many containers.</td></tr><tr><td>SECSERVER_CACHE_CIPHER_KEY_FILE</td><td>Cipher key for cache file.</td><td>/docker.d/secserver.cache/configcachecipherkey.bin</td></tr><tr><td>SECSERVER_CACHE_CIPHER_MODE</td><td>Cipher mode for cache file.</td><td>true</td></tr><tr><td>SECSERVER_CACHE_FILE</td><td>Ciphered CACHE.</td><td>/docker.d/secserver.cache/configcachefile.json</td></tr><tr><td>SECSERVER_ID</td><td>Server identifier.</td><td>Host name of container.</td></tr><tr><td>SECSERVER_INET_NAME</td><td>iNET server name.</td><td>Required parameter for Sign&go WebServer, WebServer ADMIN, WebServer USER.</br>Same value as the identifier value for Sign&go SecurityServer.</td></tr><tr><td>STATIC_CONFIG_ENV</td><td>Environment mode.</td><td>Customization is disabled<br/>To enable it, set the following value: sng.config.environment=DEV (where DEV is your environment)</td></tr></tbody></table>

## Notes

### (1) Used on initialization

Some parameters such as product license must be set to ensure a valid configuration on initialization.<br/>
These parameters may be read by other components. Moreover each component has its own start cycle. Therefore we highly
recommend to set this variable when no configuration has been deployed before, i.e., first configuration database
container run.<br/>
You can also change the value later in the configuration database, but you may need to restart some components.<br/>

### (2) Mount point available

In order to configure huge string or binary content, a mount point can be set with expected format (see <b>Customization
by mount points</b>).

### (3) Used for password encryption

Since version 8.2, it's possible to configure a specific directory which contains cipher keys for password
encryption.<br/>
This customization implies you share the cipher key with all containers which require access to the configuration
database.<br/>
On initialization, if no key is properly defined, a random key is generated and available in a persistent volume (
see <b>Output files or mount points)</b>.<br/>
A new cipher configuration can be generated through the WEB UI (Administration application > Configuration tab).<br/>

### (4) New value generated

When no value is defined, the container generates a random value in specific directories (see <b>Output files or mount
points)</b>).

### (5) Value computed according to JDBC protocol

The value varies according to a JDBC URL parameter. The required database protocol is guessed by JDBC URL parsing which
determines what value has to be used.

<table cellpadding="10" border="1">
	<thead>
		<tr>
			<th align="left">Variable name</th>
			<th align="left">Database engine</th>
			<th align="left">Value</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>DATASOURCE_LOGS_LOGIN<br/>DATASOURCE_TECH_LOGIN</td>
			<td>postgresql<br/>mysql<br/>sqlserver<br/>oracle<br/>derby</td>
            <td>postgres<br/>root<br/>sa<br/>sysdba<br/>sa</td>
		</tr>
		<tr>
			<td>DATASOURCE_LOGS_DRIVER<br/>DATASOURCE_TECH_DRIVER</td>
			<td>postgresql<br/>mysql<br/>sqlserver<br/>oracle<br/>derby</td>
            <td>org.postgresql.Driver<br/>com.mysql.cj.jdbc.Driver<br/>com.microsoft.sqlserver.jdbc.SQLServerDriver<br/>oracle.jdbc.driver.OracleDriver<br/>com.ilex.signandgo.commonspring.dialect.DerbyDialectSng
org.hibernate.dialect.DerbyTenFiveDialect
org.hibernate.dialect.DerbyTenSevenDialect
org.hibernate.dialect.DerbyTenSixDialect</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>postgresql</td>
			<td>DATASOURCE_CONFIG_DIALECT=com.ilex.signandgo.commonspring.dialect.PostgreSQL81DialectSng<br/>DATASOURCE_CONFIG_DRIVER=org.postgresql.Driver<br/>DATASOURCE_CONFIG_LOGIN=postgres</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>mysql</td>
			<td>DATASOURCE_CONFIG_DIALECT=org.hibernate.dialect.MySQL8Dialect<br/>DATASOURCE_CONFIG_DRIVER=com.mysql.cj.jdbc.Driver<br/>DATASOURCE_CONFIG_LOGIN=root</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>sqlserver</td>
			<td>DATASOURCE_CONFIG_DIALECT=org.hibernate.dialect.SQLServer2012Dialect<br/>DATASOURCE_CONFIG_DRIVER=com.microsoft.sqlserver.jdbc.SQLServerDriver<br/>DATASOURCE_CONFIG_LOGIN=sa</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>oracle</td>
			<td>DATASOURCE_CONFIG_DIALECT=org.hibernate.dialect.Oracle12cDialect<br/>DATASOURCE_CONFIG_DRIVER=oracle.jdbc.driver.OracleDriver<br/>DATASOURCE_CONFIG_LOGIN=sysdba</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>derby</td>
			<td>DATASOURCE_CONFIG_DIALECT=com.ilex.signandgo.commonspring.dialect.DerbyDialectSng<br/>DATASOURCE_CONFIG_DRIVER=org.apache.derby.jdbc.ClientDriver<br/>DATASOURCE_CONFIG_LOGIN=user</td>
		</tr>
	</tbody>
</table>

### (6) See the supported JDBC dialects

<table cellpadding="10" border="1">
	<thead>
		<tr>
			<th align="left">Database engine</th>
			<th align="left">Supported dialects (*)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>postgresql</td>
			<td>org.hibernate.dialect.PostgresPlusDialect<br/>org.hibernate.dialect.PostgreSQL81Dialect<br/>com.ilex.signandgo.commonspring.dialect.PostgreSQL81DialectSng<br/>org.hibernate.dialect.PostgreSQL91Dialect<br/>org.hibernate.dialect.PostgreSQL9Dialect<br/>org.hibernate.dialect.PostgreSQL82Dialect<br/>org.hibernate.dialect.PostgreSQL92Dialect<br/>org.hibernate.dialect.PostgreSQL93Dialect<br/>org.hibernate.dialect.PostgreSQL94Dialect<br/>org.hibernate.dialect.PostgreSQL95Dialect<br/>org.hibernate.dialect.PostgreSQL10Dialect</td>
		</tr>
		<tr>
			<td>mysql</td>
			<td>org.hibernate.dialect.MySQL8Dialect<br/>org.hibernate.dialect.MySQLInnoDBDialect<br/>org.hibernate.dialect.MySQLMyISAMDialect<br/>org.hibernate.dialect.MySQL57InnoDBDialect<br/>org.hibernate.dialect.MySQL5InnoDBDialect<br/>org.hibernate.dialect.MySQL5Dialect<br/>org.hibernate.dialect.MySQLDialect<br/>org.hibernate.dialect.MySQL55Dialect<br/>org.hibernate.dialect.MySQL57Dialect</td>
		</tr>
		<tr>
			<td>sqlserver</td>
			<td>org.hibernate.dialect.SQLServerDialect<br/>org.hibernate.dialect.SQLServer2012Dialect<br/>org.hibernate.dialect.SQLServer2005Dialect<br/>org.hibernate.dialect.SQLServer2008Dialect</td>
		</tr>
		<tr>
			<td>oracle</td>
			<td>org.hibernate.dialect.OracleDialect<br/>org.hibernate.dialect.Oracle12cDialect<br/>org.hibernate.dialect.Oracle8iDialect<br/>org.hibernate.dialect.Oracle10gDialect<br/>org.hibernate.dialect.Oracle9Dialect<br/>org.hibernate.dialect.Oracle9iDialect</td>
		</tr>
		<tr>
			<td>derby</td>
			<td>com.ilex.signandgo.commonspring.dialect.DerbyDialectSng<br/>org.hibernate.dialect.DerbyTenSevenDialect<br/>org.hibernate.dialect.DerbyTenFiveDialect<br/>org.hibernate.dialect.DerbyTenSixDialect</td>
		</tr>
	</tbody>
</table>

(*) See details the JDBC driver documentation to ensure compatibility. If any doubt, use default dialect.


# Customization by mount points

## Expected files or mount points
<table cellpadding="10" border="1"><thead><tr><th align="left">Path</th><th align="left">Description</th><th align="left">Note</th></tr></thead><tbody><tr><td>/docker.d/cipher.configurationdb</td><td>Default directory for cipher key storage</br>For further information, please refer to Documentation portal > Integration</td><td>Can be changed with CIPHER_CONFIG_KEYS_DIR.</td></tr><tr><td>/docker.d/product.licenses/signandgo.license.key</td><td>Default file path for Sign&go product license (TXT format).</td><td>Useful for initialization but can be set dynamically through WEB UI (Administration application > Configuration tab).</td></tr><tr><td>/opt/signandgo/server/techconfcustom.xml</td><td>File for technical configuration customization.<br/>For further information, please refer to Documentation portal > Integration</td><td>File for security server.</td></tr><tr><td>/docker.d/custom.scripts</td><td>Special directory for custom scripts</td><td>See <b>Customization by scripts</b></td></tr><tr><td>/opt/signandgo/server/certManager/Certificates</td><td>CA certificates folder used by CRL files.</td><td>For further information on RFID cards and Sign&go tokens, please refer to Documentation portal > Integration.</td></tr><tr><td>/opt/signandgo/server/monitoring-cache-conf.xml</td><td>Monitoring cache configuration.</td><td>File for security server.</td></tr><tr><td>/opt/signandgo/server/cache.xml</td><td>Identity cache configuration</td><td>For further information on RFID cards and Sign&go tokens, please refer to Documentation portal > Integration.</td></tr><tr><td>/docker.d/cipher.configurationdb/default.docker.generated.bin</td><td>Random key for cipher key storage.<br/>For further information, please refer to Documentation portal > Integration</td><td>If customization variables have not been set.</td></tr><tr><td>/docker.d/secserver.trust/root.jks</td><td>root.jks genway</td><td>/</td></tr><tr><td>/docker.d/secserver.trust/server.p12</td><td>server.p12 genway</td><td>/</td></tr></tbody></table>

## Output files or mount points
<table cellpadding="10" border="1"><thead><tr><th align="left">Path</th><th align="left">Description</th><th align="left">Note</th></tr></thead><tbody><tr><td>/docker.d/secserver.cache/configcachefile.json</td><td>Configuration cache file</td><td>Cannot be shared by multiple container instances.<br/>Used for standalone mode.</td></tr><tr><td>/docker.d/cipher.configurationdb</td><td>Default directory for cipher key storage</br>For further information, please refer to Documentation portal > Integration</td><td>Can be changed with CIPHER_CONFIG_KEYS_DIR.</td></tr><tr><td>/docker.d/container.logs</td><td>Container log files.</td><td>Cannot be shared by multiple container instances.</td></tr><tr><td>/docker.d/secserver.cache/configcachecipherkey.bin</td><td>Configuration cache cipher key</td><td>Cannot be shared by multiple container instances.<br/>Used for standalone mode.</td></tr><tr><td>/docker.d/secserver.cache</td><td>Default directory for configuration cache</td><td>Cannot be shared by multiple container instances.</td></tr><tr><td>/opt/signandgo/server/certManager/CRL</td><td>Working directory for CRL files</td><td>Cannot be shared by multiple container instances.</td></tr><tr><td>/docker.d/audit.persistent.logs</td><td>Audit persistent logs.<br/>For further information, please refer to Administration application contextual help > Audit.</td><td>Can be changed with AUDIT_PERSISTENT_LOGS.</td></tr><tr><td>/docker.d/secserver.trust</td><td>Default directory where SSL artifacts for security server are generated</td><td>Generation done only if SSL required files do not exist.</td></tr></tbody></table>

# Customization by scripts

You can mount custom scripts in the directory /docker.d/custom.scripts.

The following rules are applied:
- Scripts are executed sequentially in alphabetical order.
- All script executions are performed at each container start, i.e., after the main docker customization and just before the server launch.
- Scripts are executed with "bash" binary.
- If a wrong modification is done using custom scripts, the server may not launch.
- All scripts are executed with docker user rights.

Default user is 'server' (UID: 1000, GID: 1000).


# Other recommendations

## It's highly recommended to increase JVM memory size following your use cases:
- Use CATALINA_OPTS for Sign&go (WebServer, WebServer ADMIN and WebServer USER)
- Use JAVA_OPTS_XMS_SIZE, JAVA_OPTS_XMX_SIZE for Sign&go SecurityServer

# Docker with command lines

## Get a repository access

Get your account credentials and sign in.

```console
docker login --username [USER_NAME] --password-stdin /
```

## Get a specific image
```console
docker pull ///[IMAGE_NAME]:[TAG]
```

## Create / launch a container

To create and launch with one docker command

```console
docker run --name myServer -p 18443:8443
    -v configuration.db:/docker.d/configuration.db
    -e DATASOURCE_CONFIG_URL=jdbc:postgresql://configuration.db:5432/sngconfiguration
    -e DATASOURCE_CONFIG_PWD=secret
    -e SECSERVER_INET_NAME=security.server
    ///signandgo-webserver-full:[TAG]
```

If your database is properly initialized, use the following URL to access the administration server: [Administration server] using https://localhost:18443/admin<br/>
A delay is required to start the Web server.

## Create a cipher configuration volume


```console
docker volume create cipher.configurationdb
docker run --rm -v cipher.configurationdb:/opt -u root debian:latest /bin/bash -c "useradd -u 1000 server && chown server:server /opt && stat /opt"
```

UID:1000, GID:1000 matches with user 'server' (default user).



# Docker compose

## Minimalist topology with 2 servers : Sign&go [WebServer and SecurityServer] (*)

(*) suppose all directories and databases are reachable
```yaml
version: '2'

networks:
  local.network:
    driver: bridge
    driver_opts:
      com.docker.network.enable_ipv6: "true"
    ipam:
      driver: default
      config:
        - subnet: 172.19.1.0/24
          gateway: 172.19.1.1
        - subnet: "2001:3984:3989::/64"
          gateway: "2001:3984:3989::1"

services:
  web.server:
    domainname: ilex-si.com
    image: ///signandgo-webserver-full:9.0.0-SNAPSHOT
    ports:
      - '18443:8443'
    environment:
      - CATALINA_OPTS=-Xms128M -Xmx1G
      - DATASOURCE_CONFIG_URL=jdbc:postgresql://configuration.db:5432/sngconfiguration
      - DATASOURCE_CONFIG_PWD=secret
      - SECSERVER_INET_NAME=security.server
    networks:
      - local.network
    volumes:
      - cipher.configurationdb:/docker.d/cipher.configurationdb
    links:
      - security.server
      - configuration.db
    depends_on:
      - configuration.db

  security.server:
    domainname: ilex-si.com
    image: ///signandgo-secserver:9.0.0-SNAPSHOT
    environment:
      - DATASOURCE_CONFIG_URL=jdbc:postgresql://configuration.db:5432/sngconfiguration
      - DATASOURCE_CONFIG_PWD=secret
      - JAVA_OPTS_XMS_SIZE=256M
      - JAVA_OPTS_XMX_SIZE=2G
    networks:
      - local.network
    volumes:
      - cipher.configurationdb:/docker.d/cipher.configurationdb

volumes:
  cipher.configurationdb:
    external: true
```

## Notes

In this case, before the stack deployment, we suppose the minimalist environment is ready, i.e.:
- [x] A configuration database for Sign&go has been initialized.
- [x] The previous instance has been initialized with a valid product license.
- [x] A technical directory database for Sign&go has been initialized.
- [x] An audit database for Sign&go has been initialized.
- [x] If a custom cipher key (password encryption) has been used for the Sign&go configuration, make sure to mount it in each container which requires access to the database.


# Build your own images with Sign&go product

## From Sign&go SecurityServer

If you want to use the docker boot (ENTRYPOINT) of official images, make sure that:
- The Linux distribution supports Bash >= 4.0.
- A java environment is available. For further information, please refer to Documentation portal > Integration).
- unzip binary is installed.
- Your environment contains the following elements:
- IX_HOME environment variable
- ${IX_HOME}/server directory


### Dockerfile example based on Sign&go SecurityServer

```dockerfile

FROM [JAVA_IMAGE]:[TAG]

ENV REQUIRED_CAPABILITIES DATASOURCE_CONFIG,SECSERVER_CONF,SECSERVER_TRUST
ENV IX_HOME /opt/signandgo

# Create a default volume in order to avoid cache lost 
VOLUME /docker.d/secserver.cache

USER root

RUN groupadd server -g 1000 && \
    useradd server -g 1000 --groups 1000

# Copy the expected structure
COPY --chown=server:server --from=///signandgo-secserver:9.0.0-SNAPSHOT \
    /opt/signandgo ${IX_HOME}

# Retrieve the content of docker boot 
# Do not modify the original content and structure of this folder (use only the available mount points)
COPY --chown=server:server --from=///signandgo-secserver:9.0.0-SNAPSHOT \
    /docker.d/ /docker.d/

## Secure the differents paths in order to permit safe boot
RUN bash /docker.d/entrypoint secure --boot && \
    bash /docker.d/entrypoint secure --security-server && \
    sudo apt update && sudo apt upgrade -y && \
    apt-get autoclean -y && apt-get clean -y && rm -rf /var/lib/apt/lists/*

USER server

EXPOSE 3102
EXPOSE 3103

ENTRYPOINT ["/bin/bash"]
CMD ["/docker.d/entrypoint", "start"]
```


# Licenses

## General license for Sign&go product
ATTENTION - PLEASE READ THE FOLLOWING LICENSE TERMS AND CONDITIONS CAREFULLY.

THE FOLLOWING LIMITED RIGHTS ARE GRANTED TO YOU:

a. A nonexclusive and non cessible license, in order to use the number of user sessions and administration servers specific to your order.

IT IS STRICTLY FORBIDDEN:

a. To modify wholly or partly the program.

b. To decompile or reconstitute the program.

c. For registered users, it is strictly forbidden to transmit their registered number to a third party.

IF YOU PERFORM ONE OF THESE ABOVE PROCESS, YOUR RIGHTS OF USE ARE AUTOMATICALLY CANCELLED. THIS CANCELLATION IS ADDED TO THE LEGAL RECOURSE PENAL, CIVIL OR OTHER, ILEX SYSTÈMES INFORMATIQUES CAN ASSERT. 


LIMITED WARRANTY:

NO LIABILITY FOR INDIRECT DAMAGE. ILEX assumes no responsibility in any circumstance of any damage whatever it may be (including but not in a limited way, the direct or indirect damage caused by the loss of trading profit, the end of business, the loss of commercial information or any other financial loss) resultant of this product use or impossibility of use. 

This program is supplied and conceded under license "AS IS", without warranty of any sort, express or tacit,including, without limitation, implicit warranty of market value and adaptability to special end. ILEX or any other third party involved in the creation, the production or the delivery of the program, assume no responsibility in any circumstance, of any direct or indirect damage including damage for loss of profit, cessation of commercial activities or other, undergone by the program user, even in the case ILEX would have been informed of such damage.

Results or performances of this program are totally assumed by the user. ILEX can revoke this license at all times by informing the program user.  The user can end the software license by destroying or deleting all copy of the program.

This limited warranty will be interpreted under french law and in agreement with the french law.

PROPERTY:

"SIGN&GO" and "MEIBO" and "INTRANEX" and "TRIPPER" are registered trademarks and are protected by french law on trademark and intellectual property. The use of the product name is strictly forbidden without ILEX specific written prior permission.

The program including its code, its documentation, its look, its structure and its organization, is a sole product of ILEX SYSTÈMES INFORMATIQUES, which permanently keeps the property rights of the program or of its copies, changes or merged parts.

NB : for further information, please refer to the software license agreement in your possession.

## Docker license
All Ilex docker scripts in /docker.d/ directory shall not be modified or used with another way(s)
<br/><br/>
As with all Docker images, these likely also contain other software which may be under other licenses (such as Apache Tomcat, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).
<br/><br/>
As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.