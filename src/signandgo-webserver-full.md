# Available images

## Sign&go server images

<table cellpadding="10" border="1" >
    <thead>
        <tr>
            <th align="left">Image name<br/>[IMAGE_NAME]</th>
            <th align="left">Description</th>
            <th align="left">Note</th>
            <th align="left">Available tags<br/>[TAG]</th>
            <th align="left">Supported architectures</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>signandgo-webserver-full</td>
            <td>Sign&go WebServer image</td>
            <td>
                Contains Sign&go Web server ADMIN and Sign&go Web server USER images, i.e., all administration and user functionalities. See below for more details.
            </td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
        <tr>
            <td>signandgo-webserver-admin</td>
            <td>Sign&go WebServer ADMIN image</td>
            <td>Contains all administration functionalities such as Configuration, Audit, Partnership, etc.</td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
        <tr>
            <td>signandgo-webserver-user</td>
            <td>Sign&go WebServer USER image</td>
            <td>Contains all user functionalities such as authentication, Sponsorship, My account, etc.</td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
        <tr>
            <td>signandgo-secserver</td>
            <td>Sign&go SecurityServer image</td>
            <td>Contains all security server functionalities.</td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
    </tbody>
</table>

## Sign&go database images

<table cellpadding="10" border="1" >
    <thead>
        <tr>
            <th align="left">Image name<br/>[IMAGE_NAME]</th>
            <th align="left">Description</th>
            <th align="left">Note</th>
            <th align="left">Available tags<br/>[TAG]</th>
            <th align="left">Supported architectures</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>signandgo-db-logs-postgres</td>
            <td>Audit database for Sign&go [postgresql] image</td>
            <td>
                Contains database with initialization schemas.<br/>
                For further information, please refer to Documentation portal > Integration.
            </td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
        <tr>
            <td>signandgo-db-technical-postgres</td>
            <td>Technical directory database for Sign&go [postgresql] image</td>
            <td>
                Contains database with initialization schemas.<br/>
                For further information, please refer to Documentation portal > Integration.
            </td>
            <td>8.2,9.0.0-SNAPSHOT</td>
            <td>amd64,arm64</td>
        </tr>
    </tbody>
</table>

# Sign&go components and images

<table cellpadding="10" border="1" >
    <thead>
        <tr>
            <th align="left">Name</th>
            <th align="left">Referenced image(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Security server</td>
            <td>Sign&go SecurityServer image</td>
        </tr>
        <tr>
            <td>Authentication and authorisation agents</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer ADMIN image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
        <tr>
            <td>Administration server</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer ADMIN image
            </td>
        </tr>
        <tr>
            <td>User directories</td>
            <td>
                Not supported by Ilex images<br/>
                Can be OpenLDAP, Active Directory (AD), etc.<br/>
                For further information, please refer to Documentation portal > Integration.
            </td>
        </tr>
        <tr>
            <td>Audit</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer ADMIN image
            </td>
        </tr>
        <tr>
            <td>Administration application</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer ADMIN image
            </td>
        </tr>
        <tr>
            <td>User portal</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
        <tr>
            <td>Mobility Center</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
        <tr>
            <td>Sponsorship</td>    
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
        <tr>
            <td>My account</td>
            <td>
                Sign&go WebServer image<br/>
                Sign&go WebServer USER image
            </td>
        </tr>
    </tbody>
</table>
<br/>

# Customization by variables

For Sign&go WebServer, WebServer ADMIN and WebServer USER images, the environment is inherited from Apache Tomcat official image.<br/>
For further information about possible customization such as CATALINA_OPTS, please refer to the [Apache Tomcat](https://hub.docker.com/_/tomcat)

## Sign&go (WebServer, WebServer ADMIN, WebServer USER) (SSL)

<table cellpadding="10" border="1"><thead><tr><th align="left">Name </th><th align="left">Description</th><th align="left">Default value</th></tr></thead><tbody><tr><td>WEBSERVER_P12_PATH</td><td>Path for server P12.</td><td>/docker.d/webserver.trust/server.p12</td></tr><tr><td>WEBSERVER_P12_PWD</td><td>Password for server P12.</td><td>secret</td></tr><tr><td>WEBSERVER_TRUSTSTORE_PATH</td><td>Path for server truststore.</td><td>/docker.d/webserver.trust/cacert.jks</td></tr><tr><td>WEBSERVER_TRUSTSTORE_PWD</td><td>Password for server truststore.</td><td>public</td></tr></tbody></table>

## Sign&go (WebServer, WebServer ADMIN, WebServer USER)

<table cellpadding="10" border="1"><thead><tr><th align="left">Name </th><th align="left">Description</th><th align="left">Default value</th></tr></thead><tbody><tr><td>AGENTS_DEFAULT_HOST</td><td>Default authorized host for all agents.</td><td>127.0.0.1<br/>(1) Used on initialization, i.e., first run of configuration database container.</td></tr><tr><td>AGENT_LOCAL_NAME</td><td>Agent name.</td><td>AGAPI</td></tr><tr><td>AGENT_LOCAL_PWD</td><td>Agent password.</td><td>secret</td></tr><tr><td>AGENT_MOBILITYCENTER_NAME</td><td>Agent name.<br/>For further information, please refer to the Mobility Center section in Documentation portal > Integration.</td><td>AGMOBILITYCENTER</td></tr><tr><td>AGENT_MOBILITYCENTER_PWD</td><td>Agent password.<br/>For further information, please refer to the Mobility Center section in Documentation portal > Integration.</td><td>secret</td></tr><tr><td>AUDIT_PERSISTENT_LOGS</td><td>Path for audit logs.</td><td>/docker.d/audit.persistent.logs</td></tr><tr><td>CIPHER_CONFIG_ALGORITHM_ID</td><td>Cipher algorithm.<br/>For further information, please refer to Documentation portal > Integration.</td><td>AES256/GCM/NoPadding<br/>(1) Used on initialization, i.e., first run of configuration database container.<br/>(3) Used for password encryption.</td></tr><tr><td>CIPHER_CONFIG_HASHKEY</td><td>Cipher hash Key.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(1) Used on initialization, i.e., first run of configuration database container.<br/>(3) Used for password encryption.</td></tr><tr><td>CIPHER_CONFIG_KEYS_DIR</td><td>Storage directory for cipher keys.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(3) Used for password encryption.</td></tr><tr><td>DATASOURCE_CONFIG_DIALECT</td><td>JDBC dialect class.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(5) Value computed according to JDBC protocol.<br/>(6) See the supported dialects for JDBC protocol.</td></tr><tr><td>DATASOURCE_CONFIG_DRIVER</td><td>JDBC driver class. For further information, please refer to Documentation portal > Integration.</td><td>(5) Value computed according to JDBC protocol.</td></tr><tr><td>DATASOURCE_CONFIG_LOGIN</td><td>User login.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(5) Value computed according to JDBC protocol.</td></tr><tr><td>DATASOURCE_CONFIG_PWD</td><td>User password.<br/>For further information, please refer to Documentation portal > Integration.</td><td>Required parameter.</td></tr><tr><td>DATASOURCE_CONFIG_URL</td><td>JDBC URL.<br/>For further information, please refer to Documentation portal > Integration.</td><td>Required parameter.</td></tr><tr><td>DATASOURCE_LOGS_DRIVER</td><td>JDBC driver class.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(1) Used on initialization, i.e., first run of configuration database container.<br/>(5) Value computed according to JDBC protocol.</td></tr><tr><td>DATASOURCE_LOGS_LOGIN</td><td>User login.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(1) Used on initialization, i.e., first run of configuration database container.<br/>(5) Value computed according to JDBC protocol.</td></tr><tr><td>DATASOURCE_LOGS_PWD</td><td>User password.<br/>For further information, please refer to Documentation portal > Integration.</td><td>secret<br/>(1) Used on initialization, i.e., first run of configuration database container.</td></tr><tr><td>DATASOURCE_LOGS_URL</td><td>JDBC URL.<br/>For further information, please refer to Documentation portal > Integration.</td><td>jdbc:postgresql://my.external.host:5432/snglogs<br/>(1) Used on initialization, i.e., first run of configuration database container.</td></tr><tr><td>DATASOURCE_TECH_DRIVER</td><td>JDBC driver class.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(1) Used on initialization, i.e., first run of configuration database container.<br/>(5) Value computed according to JDBC protocol.</td></tr><tr><td>DATASOURCE_TECH_LOGIN</td><td>User login.<br/>For further information, please refer to Documentation portal > Integration.</td><td>(1) Used on initialization, i.e., first run of configuration database container.<br/>(5) Value computed according to JDBC protocol.</td></tr><tr><td>DATASOURCE_TECH_PWD</td><td>User password.<br/>For further information, please refer to Documentation portal > Integration.</td><td>secret<br/>(1) Used on initialization, i.e., first run of configuration database container.</td></tr><tr><td>DATASOURCE_TECH_URL</td><td>JDBC URL.<br/>For further information, please refer to Documentation portal > Integration.</td><td>jdbc:postgresql://my.external.host:5432/sngcredentialsandtechnicaldata<br/>(1) Used on initialization, i.e., first run of configuration database container.</td></tr><tr><td>LOGS_APPLICATION_ID</td><td>Application ID (logs persistent files).</td><td>The FQDN of current container.<br/>Shall be unique when audit persistent folder is shared between many containers.</td></tr><tr><td>SECSERVER_INET_NAME</td><td>iNET server name.</td><td>Required parameter for Sign&go WebServer, WebServer ADMIN, WebServer USER.</br>Same value as the identifier value for Sign&go SecurityServer.</td></tr><tr><td>SECSERVER_INET_PORT</td><td>iNET server main port.</td><td>3102</td></tr><tr><td>SIGNANDGO_LICENSE</td><td>Product license.</td><td>(1) Used on initialization, i.e., first run of configuration database container.<br/>(2) Mount point available.</td></tr><tr><td>STATIC_CONFIG_ENV</td><td>Environment mode.</td><td>Customization is disabled<br/>To enable it, set the following value: sng.config.environment=DEV (where DEV is your environment)</td></tr><tr><td>USER_URL</td><td>Sign&go WebServer USER URL.</td><td>/user<br/>(1) Used on initialization, i.e., first run of configuration database container.</td></tr><tr><td>WEBSERVER_ADMIN_APPNAME</td><td>Resource name.</td><td>admin</td></tr><tr><td>WEBSERVER_ADMIN_BASIC_HTTP_LOGIN</td><td>Basic HTTP account login (super admin).</td><td>admin.basic.http</td></tr><tr><td>WEBSERVER_ADMIN_BASIC_HTTP_PWD</td><td>Basic HTTP account password (super admin).</td><td>(4) New value generated.</td></tr><tr><td>WEBSERVER_CACHE_MAX_SIZE</td><td>Cache maximum size in "server.xml" (in bytes).</td><td>500000</td></tr><tr><td>WEBSERVER_USER_APPNAME</td><td>Resource name.</td><td>user</td></tr><tr><td>WEBSERVER_USER_BASIC_HTTP_LOGIN</td><td>Basic HTTP account login (manage health resource).</td><td>user.basic.http</td></tr><tr><td>WEBSERVER_USER_BASIC_HTTP_PWD</td><td>Basic HTTP account login (manage health resource).</td><td>(4) New value generated.</td></tr></tbody></table>

## Notes

### (1) Used on initialization

Some parameters such as product license must be set to ensure a valid configuration on initialization.<br/>
These parameters may be read by other components. Moreover each component has its own start cycle. Therefore we highly
recommend to set this variable when no configuration has been deployed before, i.e., first configuration database
container run.<br/>
You can also change the value later in the configuration database, but you may need to restart some components.<br/>

### (2) Mount point available

In order to configure huge string or binary content, a mount point can be set with expected format (see <b>Customization
by mount points</b>).

### (3) Used for password encryption

Since version 8.2, it's possible to configure a specific directory which contains cipher keys for password
encryption.<br/>
This customization implies you share the cipher key with all containers which require access to the configuration
database.<br/>
On initialization, if no key is properly defined, a random key is generated and available in a persistent volume (
see <b>Output files or mount points)</b>.<br/>
A new cipher configuration can be generated through the WEB UI (Administration application > Configuration tab).<br/>

### (4) New value generated

When no value is defined, the container generates a random value in specific directories (see <b>Output files or mount
points)</b>).

### (5) Value computed according to JDBC protocol

The value varies according to a JDBC URL parameter. The required database protocol is guessed by JDBC URL parsing which
determines what value has to be used.

<table cellpadding="10" border="1">
	<thead>
		<tr>
			<th align="left">Variable name</th>
			<th align="left">Database engine</th>
			<th align="left">Value</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>DATASOURCE_LOGS_LOGIN<br/>DATASOURCE_TECH_LOGIN</td>
			<td>postgresql<br/>mysql<br/>sqlserver<br/>oracle<br/>derby</td>
            <td>postgres<br/>root<br/>sa<br/>sysdba<br/>sa</td>
		</tr>
		<tr>
			<td>DATASOURCE_LOGS_DRIVER<br/>DATASOURCE_TECH_DRIVER</td>
			<td>postgresql<br/>mysql<br/>sqlserver<br/>oracle<br/>derby</td>
            <td>org.postgresql.Driver<br/>com.mysql.cj.jdbc.Driver<br/>com.microsoft.sqlserver.jdbc.SQLServerDriver<br/>oracle.jdbc.driver.OracleDriver<br/>com.ilex.signandgo.commonspring.dialect.DerbyDialectSng
org.hibernate.dialect.DerbyTenFiveDialect
org.hibernate.dialect.DerbyTenSevenDialect
org.hibernate.dialect.DerbyTenSixDialect</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>postgresql</td>
			<td>DATASOURCE_CONFIG_DIALECT=com.ilex.signandgo.commonspring.dialect.PostgreSQL81DialectSng<br/>DATASOURCE_CONFIG_DRIVER=org.postgresql.Driver<br/>DATASOURCE_CONFIG_LOGIN=postgres</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>mysql</td>
			<td>DATASOURCE_CONFIG_DIALECT=org.hibernate.dialect.MySQL8Dialect<br/>DATASOURCE_CONFIG_DRIVER=com.mysql.cj.jdbc.Driver<br/>DATASOURCE_CONFIG_LOGIN=root</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>sqlserver</td>
			<td>DATASOURCE_CONFIG_DIALECT=org.hibernate.dialect.SQLServer2012Dialect<br/>DATASOURCE_CONFIG_DRIVER=com.microsoft.sqlserver.jdbc.SQLServerDriver<br/>DATASOURCE_CONFIG_LOGIN=sa</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>oracle</td>
			<td>DATASOURCE_CONFIG_DIALECT=org.hibernate.dialect.Oracle12cDialect<br/>DATASOURCE_CONFIG_DRIVER=oracle.jdbc.driver.OracleDriver<br/>DATASOURCE_CONFIG_LOGIN=sysdba</td>
		</tr>
		<tr>
			<td>DATASOURCE_CONFIG_XXX</td>
			<td>derby</td>
			<td>DATASOURCE_CONFIG_DIALECT=com.ilex.signandgo.commonspring.dialect.DerbyDialectSng<br/>DATASOURCE_CONFIG_DRIVER=org.apache.derby.jdbc.ClientDriver<br/>DATASOURCE_CONFIG_LOGIN=user</td>
		</tr>
	</tbody>
</table>

### (6) See the supported JDBC dialects

<table cellpadding="10" border="1">
	<thead>
		<tr>
			<th align="left">Database engine</th>
			<th align="left">Supported dialects (*)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>postgresql</td>
			<td>org.hibernate.dialect.PostgresPlusDialect<br/>org.hibernate.dialect.PostgreSQL81Dialect<br/>com.ilex.signandgo.commonspring.dialect.PostgreSQL81DialectSng<br/>org.hibernate.dialect.PostgreSQL91Dialect<br/>org.hibernate.dialect.PostgreSQL9Dialect<br/>org.hibernate.dialect.PostgreSQL82Dialect<br/>org.hibernate.dialect.PostgreSQL92Dialect<br/>org.hibernate.dialect.PostgreSQL93Dialect<br/>org.hibernate.dialect.PostgreSQL94Dialect<br/>org.hibernate.dialect.PostgreSQL95Dialect<br/>org.hibernate.dialect.PostgreSQL10Dialect</td>
		</tr>
		<tr>
			<td>mysql</td>
			<td>org.hibernate.dialect.MySQL8Dialect<br/>org.hibernate.dialect.MySQLInnoDBDialect<br/>org.hibernate.dialect.MySQLMyISAMDialect<br/>org.hibernate.dialect.MySQL57InnoDBDialect<br/>org.hibernate.dialect.MySQL5InnoDBDialect<br/>org.hibernate.dialect.MySQL5Dialect<br/>org.hibernate.dialect.MySQLDialect<br/>org.hibernate.dialect.MySQL55Dialect<br/>org.hibernate.dialect.MySQL57Dialect</td>
		</tr>
		<tr>
			<td>sqlserver</td>
			<td>org.hibernate.dialect.SQLServerDialect<br/>org.hibernate.dialect.SQLServer2012Dialect<br/>org.hibernate.dialect.SQLServer2005Dialect<br/>org.hibernate.dialect.SQLServer2008Dialect</td>
		</tr>
		<tr>
			<td>oracle</td>
			<td>org.hibernate.dialect.OracleDialect<br/>org.hibernate.dialect.Oracle12cDialect<br/>org.hibernate.dialect.Oracle8iDialect<br/>org.hibernate.dialect.Oracle10gDialect<br/>org.hibernate.dialect.Oracle9Dialect<br/>org.hibernate.dialect.Oracle9iDialect</td>
		</tr>
		<tr>
			<td>derby</td>
			<td>com.ilex.signandgo.commonspring.dialect.DerbyDialectSng<br/>org.hibernate.dialect.DerbyTenSevenDialect<br/>org.hibernate.dialect.DerbyTenFiveDialect<br/>org.hibernate.dialect.DerbyTenSixDialect</td>
		</tr>
	</tbody>
</table>

(*) See details the JDBC driver documentation to ensure compatibility. If any doubt, use default dialect.


# Customization by mount points

## Expected files or mount points
<table cellpadding="10" border="1"><thead><tr><th align="left">Path</th><th align="left">Description</th><th align="left">Note</th></tr></thead><tbody><tr><td>/usr/local/tomcat/webapps/admin/WEB-INF/classes/techconfcustom.xml</td><td>File for technical configuration customization.<br/>For further information, please refer to Documentation portal > Integration</td><td>File for administration server.</td></tr><tr><td>/docker.d/cipher.configurationdb</td><td>Default directory for cipher key storage</br>For further information, please refer to Documentation portal > Integration</td><td>Can be changed with CIPHER_CONFIG_KEYS_DIR.</td></tr><tr><td>/usr/local/tomcat/webapps/admin/WEB-INF/classes/monitoring-cache-conf.xml</td><td>Monitoring cache configuration.</td><td>File for administration server.</td></tr><tr><td>/docker.d/product.licenses/signandgo.license.key</td><td>Default file path for Sign&go product license (TXT format).</td><td>Useful for initialization but can be set dynamically through WEB UI (Administration application > Configuration tab).</td></tr><tr><td>/docker.d/custom.scripts</td><td>Special directory for custom scripts</td><td>See <b>Customization by scripts</b></td></tr><tr><td>/docker.d/webserver.trust/cacert.jks</td><td>Default file path for web server truststore.<br/>For further information, please refer to Documentation portal > Integration</td><td>Can be changed with WEBSERVER_TRUSTSTORE_PATH.</td></tr><tr><td>/docker.d/webserver.trust/server.p12</td><td>Default file path for web server P12.<br/>For further information, please refer to Documentation portal > Integration</td><td>Can be changed with WEBSERVER_P12_PATH.</td></tr><tr><td>/docker.d/cipher.configurationdb/default.docker.generated.bin</td><td>Random key for cipher key storage.<br/>For further information, please refer to Documentation portal > Integration</td><td>If customization variables have not been set.</td></tr></tbody></table>

## Output files or mount points
<table cellpadding="10" border="1"><thead><tr><th align="left">Path</th><th align="left">Description</th><th align="left">Note</th></tr></thead><tbody><tr><td>/docker.d/generated.credentials</td><td>Basic http account credentials.</td><td>If customization variables have not been set.</td></tr><tr><td>/docker.d/webserver.trust</td><td>Default directory for Web server SSL artifacts.</td><td>Generation done only if SSL required files do not exist.</td></tr><tr><td>/docker.d/cipher.configurationdb</td><td>Default directory for cipher key storage</br>For further information, please refer to Documentation portal > Integration</td><td>Can be changed with CIPHER_CONFIG_KEYS_DIR.</td></tr><tr><td>/docker.d/container.logs</td><td>Container log files.</td><td>Cannot be shared by multiple container instances.</td></tr><tr><td>/docker.d/audit.persistent.logs</td><td>Audit persistent logs.<br/>For further information, please refer to Administration application contextual help > Audit.</td><td>Can be changed with AUDIT_PERSISTENT_LOGS.</td></tr></tbody></table>

# Customization by scripts

You can mount custom scripts in the directory /docker.d/custom.scripts.

The following rules are applied:
- Scripts are executed sequentially in alphabetical order.
- All script executions are performed at each container start, i.e., after the main docker customization and just before the server launch.
- Scripts are executed with "bash" binary.
- If a wrong modification is done using custom scripts, the server may not launch.
- All scripts are executed with docker user rights.

Default user is 'server' (UID: 1000, GID: 1000).


# Other recommendations

## It's highly recommended to increase JVM memory size following your use cases:
- Use CATALINA_OPTS for Sign&go (WebServer, WebServer ADMIN and WebServer USER)
- Use JAVA_OPTS_XMS_SIZE, JAVA_OPTS_XMX_SIZE for Sign&go SecurityServer

# Docker with command lines

## Get a repository access

Get your account credentials and sign in.

```console
docker login --username [USER_NAME] --password-stdin /
```

## Get a specific image
```console
docker pull ///[IMAGE_NAME]:[TAG]
```

## Create / launch a container

To create and launch with one docker command

```console
docker run --name myServer -p 18443:8443
    -v configuration.db:/docker.d/configuration.db
    -e DATASOURCE_CONFIG_URL=jdbc:postgresql://configuration.db:5432/sngconfiguration
    -e DATASOURCE_CONFIG_PWD=secret
    -e SECSERVER_INET_NAME=security.server
    ///signandgo-webserver-full:[TAG]
```

If your database is properly initialized, use the following URL to access the administration server: [Administration server] using https://localhost:18443/admin<br/>
A delay is required to start the Web server.

## Create a cipher configuration volume


```console
docker volume create cipher.configurationdb
docker run --rm -v cipher.configurationdb:/opt -u root debian:latest /bin/bash -c "useradd -u 1000 server && chown server:server /opt && stat /opt"
```

UID:1000, GID:1000 matches with user 'server' (default user).



# Docker compose

## Topology with 2 servers : Sign&go [WebServer and SecurityServer]

```yaml
version: '2'

networks:
  local.network:
    driver: bridge
    driver_opts:
      com.docker.network.enable_ipv6: "true"
    ipam:
      driver: default
      config:
        - subnet: 172.19.1.0/24
          gateway: 172.19.1.1
        - subnet: "2001:3984:3989::/64"
          gateway: "2001:3984:3989::1"

services:
  web.server:
    domainname: ilex-si.com
    image: ///signandgo-webserver-full:9.0.0-SNAPSHOT
    ports:
      - '18443:8443'
    environment:
      - CATALINA_OPTS=-Xms128M -Xmx1G
      - DATASOURCE_CONFIG_URL=jdbc:postgresql://configuration.db:5432/sngconfiguration
      - DATASOURCE_CONFIG_PWD=secret
      - SECSERVER_INET_NAME=security.server
      - DATASOURCE_TECH_URL=jdbc:postgresql://technical.db:5432/sngcredentialsandtechnicaldata
      - DATASOURCE_TECH_LOGIN=snguser
      - DATASOURCE_TECH_PWD=secret
      - DATASOURCE_LOGS_URL=jdbc:postgresql://logs.db:5432/snglogs
      - DATASOURCE_LOGS_LOGIN=snguser
      - DATASOURCE_LOGS_PWD=secret
      - AGENTS_DEFAULT_HOST=172.19.1.*
    networks:
      - local.network
    volumes:
      - /Users/signandgo/docker-workspace/Docker/docker-mounts/signandgo.license:/docker.d/product.licenses/signandgo.license.key
      - cipher.configurationdb:/docker.d/cipher.configurationdb
    links:
      - openldap.users
      - security.server
      - configuration.db
      - technical.db
      - logs.db
    depends_on:
      - configuration.db

  security.server:
    domainname: ilex-si.com
    image: ///signandgo-secserver:9.0.0-SNAPSHOT
    environment:
      - SECSERVER_ID=security.server
      - DATASOURCE_CONFIG_URL=jdbc:postgresql://configuration.db:5432/sngconfiguration
      - DATASOURCE_CONFIG_PWD=secret
      - JAVA_OPTS_XMS_SIZE=256M
      - JAVA_OPTS_XMX_SIZE=2G
    networks:
      - local.network
    volumes:
      - cipher.configurationdb:/docker.d/cipher.configurationdb
    links:
      - openldap.users
      - technical.db
      - configuration.db
    depends_on:
      - configuration.db
  configuration.db:
    image: postgres:13
    environment:
      - POSTGRES_DB=sngconfiguration
      - POSTGRES_PASSWORD=secret
    networks:
      - local.network
  technical.db:
    image: ///signandgo-db-technical-postgres:9.0.0-SNAPSHOT
    environment:
      - POSTGRES_PASSWORD=secret
    networks:
      - local.network
  logs.db:
    image: ///signandgo-db-logs-postgres:9.0.0-SNAPSHOT
    environment:
      - POSTGRES_PASSWORD=secret
    networks:
      - local.network

# ##############################################################
# Uncomment if you want to use an OpenLDAP directory for test
# ##############################################################
#  openldap.users:
#    image: bitnami/openldap:2
#    environment:
#      - LDAP_ADMIN_USERNAME=admin
#      - LDAP_ADMIN_PASSWORD=adminpassword
#      - LDAP_USERS=user01,user02
#      - LDAP_PASSWORDS=password1,password2
#      - LDAP_ROOT=dc=ilex-si,dc=com
#    networks:
#      - local.network
# ##############################################################

volumes:
  cipher.configurationdb:
    external: true
```


# Building your own images based on Sign&go images

### Using Sign&go WebServer

If you want to use the docker boot (ENTRYPOINT) of official images, make sure that:

- The Linux distribution supports Bash >= 4.0.
- Your Web server is based on Apache Tomcat, i.e. contains:
  - CATALINA_HOME environment variable
  - ${CATALINA_HOME}/conf directory
  - ${CATALINA_HOME}/webapps directory
  - ${CATALINA_HOME}/lib directory
  - ${CATALINA_HOME}/logs directory
- You use a valid configuration (see the paragraph below).

### Valid configurations

The docker boot scripts evaluate the REQUIRED_CAPABILITIES variable to build the expected customization.
After that, the scripts expect to update the Web applications configuration.
In other words, there is a strong link between REQUIRED_CAPABILITIES value and the expected Web applications (webapps content folder).

<table cellpadding="10" border="1">
  <thead>
    <tr>
      <th align="left">REQUIRED_CAPABILITIES</th>
      <th align="left">WEBAPPS</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td>
            IMPORT_CONFIG<br/>
            DATASOURCE_CONFIG<br/>
            DATASOURCE_LOGS<br/>
            WEBSERVER_TRUST<br/>
            WEBSERVER_CONF<br/>
            WEBSERVER_ADMIN<br/>
            AGENT_LOCAL_AUTH<br/>
        </td>
        <td>
            admin<br/>
            clusterManager<br/>
            portalAdmin<br/>
            logs
        </td>
    </tr>
    <tr>
        <td>
            DATASOURCE_CONFIG<br/>
            WEBSERVER_TRUST<br/>
            WEBSERVER_CONF<br/>
            WEBSERVER_USER<br/>
            AGENT_LOCAL_AUTH<br/>
            AGENT_MOBILITYCENTER_AUTH<br/>
            DO_USER_AUTH<br/>
        </td>
        <td>
            user<br/>
            mobilitycenter<br/>
            portalUser<br/>
            samlv2<br/>
            webservice
        </td>
    </tr>
    <tr>
        <td>
            IMPORT_CONFIG<br/>
            DATASOURCE_CONFIG<br/>
            DATASOURCE_LOGS<br/>
            WEBSERVER_TRUST<br/>
            WEBSERVER_CONF<br/>
            WEBSERVER_ADMIN<br/>
            WEBSERVER_USER<br/>
            AGENT_LOCAL_AUTH<br/>
            AGENT_MOBILITYCENTER_AUTH<br/>
            DO_USER_AUTH<br/>
        </td>
        <td>
            admin<br/>
            clusterManager<br/>
            portalAdmin<br/>
            logs<br/>
            user<br/>
            mobilitycenter<br/>
            portalUser<br/>
            samlv2<br/>
            webservice
        </td>
    </tr>
  </tbody>
</table>

### Dockerfile example based on Sign&go WebServer

```dockerfile

#syntax=docker/dockerfile:1.3
FROM [TOMCAT_IMAGE]:[TAG]

USER root

ENV REQUIRED_CAPABILITIES <USE_A_VALID_CONFIGURATION>

# Create a default volume in order to avoid generated cipher key lost 
VOLUME /docker.d/cipher.configurationdb

# Add a default user with restricted rights
RUN groupadd server -g 1000 && \
    useradd server -g 1000 --groups 1000

# Retrieve the content of deployed web archives
# Select the expected webapps
COPY --chown=server:server --from=///signandgo-webserver-full:9.0.0-SNAPSHOT \
    /usr/local/tomcat/webapps ${CATALINA_HOME}/webapps

#Retrieve the content of deployed web archives
COPY --chown=server:server --from=///signandgo-webserver-full:9.0.0-SNAPSHOT \
    /usr/local/tomcat/conf ${CATALINA_HOME}/conf

# Required libraries
COPY --chown=server:server --from=///signandgo-webserver-full:9.0.0-SNAPSHOT \
    /usr/local/tomcat/lib/sng-* ${CATALINA_HOME}/lib

# Required libraries
COPY --chown=server:server --from=///signandgo-webserver-full:9.0.0-SNAPSHOT\
    /usr/local/tomcat/lib/log4j-* ${CATALINA_HOME}/lib

# Retrieve the content of docker boot 
# Do not modify the original content and structure of this folder (use only the available mount points)
COPY --chown=server:server --from=///signandgo-webserver-full:9.0.0-SNAPSHOT \
    /docker.d /docker.d/

## Secure the differents paths in order to permit safe boot
RUN bash /docker.d/entrypoint secure --boot && \
    bash /docker.d/entrypoint secure --web-server && \
    sudo apt update && sudo apt upgrade -y && \
    apt-get autoclean -y && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# Use the default user
USER server

EXPOSE 8443

ENTRYPOINT ["/bin/bash"]
CMD ["/docker.d/entrypoint", "start"]
```

# Licenses

## General license for Sign&go product
ATTENTION - PLEASE READ THE FOLLOWING LICENSE TERMS AND CONDITIONS CAREFULLY.

THE FOLLOWING LIMITED RIGHTS ARE GRANTED TO YOU:

a. A nonexclusive and non cessible license, in order to use the number of user sessions and administration servers specific to your order.

IT IS STRICTLY FORBIDDEN:

a. To modify wholly or partly the program.

b. To decompile or reconstitute the program.

c. For registered users, it is strictly forbidden to transmit their registered number to a third party.

IF YOU PERFORM ONE OF THESE ABOVE PROCESS, YOUR RIGHTS OF USE ARE AUTOMATICALLY CANCELLED. THIS CANCELLATION IS ADDED TO THE LEGAL RECOURSE PENAL, CIVIL OR OTHER, ILEX SYSTÈMES INFORMATIQUES CAN ASSERT. 


LIMITED WARRANTY:

NO LIABILITY FOR INDIRECT DAMAGE. ILEX assumes no responsibility in any circumstance of any damage whatever it may be (including but not in a limited way, the direct or indirect damage caused by the loss of trading profit, the end of business, the loss of commercial information or any other financial loss) resultant of this product use or impossibility of use. 

This program is supplied and conceded under license "AS IS", without warranty of any sort, express or tacit,including, without limitation, implicit warranty of market value and adaptability to special end. ILEX or any other third party involved in the creation, the production or the delivery of the program, assume no responsibility in any circumstance, of any direct or indirect damage including damage for loss of profit, cessation of commercial activities or other, undergone by the program user, even in the case ILEX would have been informed of such damage.

Results or performances of this program are totally assumed by the user. ILEX can revoke this license at all times by informing the program user.  The user can end the software license by destroying or deleting all copy of the program.

This limited warranty will be interpreted under french law and in agreement with the french law.

PROPERTY:

"SIGN&GO" and "MEIBO" and "INTRANEX" and "TRIPPER" are registered trademarks and are protected by french law on trademark and intellectual property. The use of the product name is strictly forbidden without ILEX specific written prior permission.

The program including its code, its documentation, its look, its structure and its organization, is a sole product of ILEX SYSTÈMES INFORMATIQUES, which permanently keeps the property rights of the program or of its copies, changes or merged parts.

NB : for further information, please refer to the software license agreement in your possession.

## Docker license
All Ilex docker scripts in /docker.d/ directory shall not be modified or used with another way(s)
<br/><br/>
As with all Docker images, these likely also contain other software which may be under other licenses (such as Apache Tomcat, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).
<br/><br/>
As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.